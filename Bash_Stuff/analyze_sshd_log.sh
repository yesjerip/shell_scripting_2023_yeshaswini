#!/bin/bash

# checking if sshd.log file is passed as argument or not
if [ $# -ne 1 ]; then
	  echo "Error:use $0 <sshd_logfile> as argument"
	    exit 1
fi

logfile=$1

# Function to count occurrences 
count() {
	  sort_count=$(sort "$1" | uniq -c | sort -nr)
	    echo "$sort_count"
    }

    # Q1: How many total connection attempts failed because of an invalid user name?
    invalid_user_attempts=$(grep "Invalid user" "$logfile" | wc -l)

    # 1(i): What are the distinct user names attempted that failed to login to the system?
    invalid_users=$(grep -o "Invalid user [^ ]*" "$logfile" | awk '{print $3}' | sort | uniq)

    # 1(ii): What are the distinct IP addresses that tried to log in with an invalid user name?
    invalid_ip_addresses=$(grep -oE "Invalid user [^ ]* from [^ ]*" "$logfile" | awk '{print $5}' | sort | uniq)

    # 1(iii): How many times was each IP address seen in the file? (BONUS: sort the IP address by number of times seen)
    ip_counts=$(grep -oE "Invalid user [^ ]* from [^ ]*" "$logfile" | awk '{print $5}' | sort | uniq -c | sort -nr)

    # Q2: How many connection attempts failed during the 'kex_exchange_identification' phase?
    kex_exchange_failures=$(grep "kex_exchange_identification" "$logfile" | wc -l)

    # 2(i): Of the 'kex_exchange_identification' errors, how many of them contain the specific error "Connection closed by remote host"?
    kex_exchange_closed_by_remote_host=$(grep "kex_exchange_identification.*Connection closed by remote host" "$logfile" | wc -l)

    # 2(ii): Of the 'kex_exchange_identification' errors, how many of them contain a different error than "Connection closed by remote host"?
    kex_exchange_other_errors=$(grep -oE "kex_exchange_identification.*[^C]onnection closed by remote host" "$logfile" | wc -l)

    # 2(iii): List out all the distinct errors from the previous answer (any error that isn't "Connection closed...").
    kex_exchange_distinct_errors=$(grep -oE "kex_exchange_identification.*[^C]onnection closed by remote host" "$logfile" | sed 's/kex_exchange_identification.*//g' | sort | uniq)

    # Q3: How many connection attempts failed during the preauth stage of login?
    preauth_failures=$(grep "preauth" "$logfile" | wc -l)

    # 3(i): Of those failing during preauth, how many are due to an invalid user name?
    preauth_invalid_user_attempts=$(grep "preauth.*Invalid user" "$logfile" | wc -l)

    # 3(i)(i): List out the distinct user names failing that failed in preauth due to invalid user name.
    preauth_invalid_users=$(grep "preauth.*Invalid user" "$logfile" | awk '{print $11}' | sort | uniq)

    # 3(ii): Of those failing during preauth, how many are due to an "auth fail" error?
    preauth_auth_failures=$(grep "preauth.*authentication failure" "$logfile" | wc -l)

    # 3(ii)(i): List out the distinct IP addresses that failed in preauth with "auth fail".
    preauth_auth_fail_ip=$(grep "preauth.*authentication failure" "$logfile" | awk '{print $9}' | sort | uniq)

    # Q4: How many connection attempts fail due to an incorrect username but not in the preauth phase?
    non_preauth_invalid_users=$(grep -oE "Invalid user [^ ]* from [^ ]*" "$logfile" | awk '{print $5}' | grep -vE "preauth" | wc -l)

    # 4(i): List out the distinct user names that are attempted here
    distinct_non_preauth_users=$(grep -oE "Invalid user [^ ]* from [^ ]*" "$logfile" | awk '{print $3}' | grep -vE "preauth" | sort | uniq)

    # 4(ii): List out the distinct IP addresses that are attempting to login here (how many attackers are there?)
    distinct_attacker_ips=$(grep -oE "Invalid user [^ ]* from [^ ]*" "$logfile" | awk '{print $5}' | grep -vE "preauth" | sort | uniq)

    # Q5: How many times does the error 'invalid protocol identifier' occur?
    invalid_protocol_identifier_count=$(grep "invalid protocol identifier" "$logfile" | wc -l)

    # 5(i): List out the distinct invalid identifiers that are used.
    distinct_invalid_identifiers=$(grep -oE "invalid protocol identifier .*" "$logfile" | awk '{print $4}' | sort | uniq)

    # Q6: How many actual, successful logins are shown in the log file?
    successful_logins=$(grep "Accepted " "$logfile" | wc -l)

    # 6(i): How many distinct users are there that successfully logged into this system?
    distinct_successful_users=$(grep "Accepted " "$logfile" | awk '{print $9}' | sort | uniq)

    # 6(ii): What are the user names of the users that logged into the system?
    successful_user_names=$(grep "Accepted " "$logfile" | awk '{print $9}')

    # 6(iii): What IP addresses did the users log in from?
    successful_ip_addresses=$(grep "Accepted " "$logfile" | awk '{print $11}')
    #(iv) Based on the information you have at hand, do you think any of the attacks succeeded? Explain why or why not you think so.


    # Output the results
    echo "1.Found $invalid_user_attempts invalid user logins"
    echo "(i)Distinct user names found:"
    count <(echo "$invalid_users")
    echo "(ii)Distinct IP addresses that tried to log in with an invalid user name:"
    count <(echo "$invalid_ip_addresses")
    echo "(iii)IP addresses seen in the file (sorted by number of times seen):"
    count <(echo "$ip_counts")
    echo "2.Connection attempts failed during the 'kex_exchange_identification' phase: $kex_exchange_failures"
    echo "(i)Of the 'kex_exchange_identification' errors, $kex_exchange_closed_by_remote_host contain the specific error 'Connection closed by remote host'"
    echo "(ii)Of the 'kex_exchange_identification' errors, $kex_exchange_other_errors contain a different error than 'Connection closed by remote host'"
    echo "(iii)Distinct errors from the 'kex_exchange_identification' phase:"
    count <(echo "$kex_exchange_distinct_errors")
    echo "3.Connection attempts failed during the preauth stage of login: $preauth_failures"
    echo "(i)Of those failing during preauth, $preauth_invalid_user_attempts are due to an invalid user name"
    echo "Distinct user names failing in preauth due to invalid user name:"
    count <(echo "$preauth_invalid_users")
    echo "(ii)Of those failing during preauth, $preauth_auth_failures are due to an 'auth fail' error"
    echo "Distinct IP addresses that failed in preauth with 'auth fail':"
    count <(echo "$preauth_auth_fail_ip")
    echo "4.Connection attempts failed due to an incorrect username but not in the preauth phase: $non_preauth_invalid_users"
    echo "(i)Distinct user names attempted in the non-preauth phase:"
    count <(echo "$distinct_non_preauth_users")
    echo "(ii)Distinct IP addresses attempting to login in the non-preauth phase:"
    count <(echo "$distinct_attacker_ips")
    echo "5.The error 'invalid protocol identifier' occurs $invalid_protocol_identifier_count times"
    echo "(i)Distinct invalid identifiers used:"
    count <(echo "$distinct_invalid_identifiers")
    echo "6.Actual successful logins shown in the log file: $successful_logins"
    echo "(i)Distinct users that successfully logged into the system:"
    count <(echo "$distinct_successful_users")
    echo "(ii)User names of the users that logged into the system:"
    count <(echo "$successful_user_names")
    echo "(iii)IP addresses from which users logged in:"
    count <(echo "$successful_ip_addresses")
    echo "(iv) Based on the analysis, it appears that there were some unsuccessful login attempts due to incorrect usernames and preauth failures.
    The log shows four successful logins by the user "port," but no information regarding their nature is provided.
    There were 6,282 failed login attempts using various usernames as shown in the output. While the log shows that attackers attempted to acquire unauthorized access,
    it is unclear whether any of these efforts were successful.
    To figure out whether any successful attacks occurred, a more thorough examination of system logs and security measures would be required."
