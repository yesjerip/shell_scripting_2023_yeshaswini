#! /bin/bash
declare -a MYARRAY=("Hello" "I'm" "Yeshaswini")
echo "Original array: ${MYARRAY[@]}"
MYARRAY[0]="Namaste!"
echo "Substituted array: ${MYARRAY[@]}"
