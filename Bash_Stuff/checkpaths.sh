for f in /etc/{skel,network,host{s,""},resolv.conf,passwd,shadow};do
	echo $f;
if [[ -f $f ]];then
	echo "$f is a file"
elif [[ -d $f ]];then
	echo "$f is a directory"
elif [[ ! -e $f ]];then
	echo "$f is not a file or directory"
else 
	echo "Neither"
fi
done
