
#!/bin/bash
 
 if [[ $# -ne 2 ]]; then
   echo "Error: Two arguments are required."
     exit 1
     fi
 
     if [[ ! -d $2 ]]; then
       echo "Error: Not a valid directory path."
         exit 1
         fi
 
         calculate_total_size() {
           file_size=$1
             if [[ -n $file_size ]]; then
                 total_size=$((total_size + file_size))
                   fi
                   }
 
                   find_files=$(find "$2" -type f -name "$1")
 
                   total_files=0
                   total_size=0
 
                   for file_path in $find_files; do
                     if [[ -f $file_path ]]; then
                         ((total_files++))
                             file_size=$(ls -la "$file_path" | awk '{print $5}')
                                 calculate_total_size "$file_size"
                                   fi
                                   done
 
                                   echo "Found $total_files number of files under directory $2, total size: $total_size bytes."
 
