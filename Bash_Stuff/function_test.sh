#! /bin/bash

function add {
    if [[ $# -ne 2 ]]; then
	            echo "Two arguments required for Addition"
		    return 1
    fi
    addition=$(($1 + $2))
    echo "Addition of two numbers is $addition"
}

add 10 20

function multiply {
	if [[ $# -ne 2 ]]; then
		echo "Two arguments needed for Multiply"
		return 1
	fi
        multiplication=$(($1 * $2))
	echo "Multiplication of two numbers is $multiplication"
}

 multiply 5 6

function factorial {
	if [[ $# -ne 1 ]]; then
           	echo "Error: factorial function requires exactly one argument."				 
		return
											
	fi
       	num=$1
        result=1	
      	while [[ $num -gt 1 ]]; do
		result=$((result * num))
		num=$((num - 1))
        done
   
echo "Factorial of $1: $result"
}
factorial 5
  
