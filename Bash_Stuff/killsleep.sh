#! /bin/bash
# Start the sleep process in the background
sleep 10 &
# Save the background sleep process's PID
sleep_pid=$!
# Wait for 5 seconds
sleep 5
# Kill the background sleep process
kill "$sleep_pid"

echo "Background sleep process is killed after 5 seconds"

