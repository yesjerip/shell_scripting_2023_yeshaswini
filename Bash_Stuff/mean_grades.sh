#! /bin/bash

if [[ -z "$1" ]]; then
	echo "Usage: <csv_file>"
	exit 1
fi

if [[ ! -f "$1" ]]; then
	echo "Error:File not found"
	exit 1
fi

#Functions for calculating sum,mean,highest,lowest

function sum_calculation {
                sum=0
	        while IFS=',' read -r _ grade; do
                         if [[ "$grade" == "Grade" ]];then
		                   continue
	                 fi

	                 sum=$(( $sum + $grade ))
	        done < "$1"
        echo "$sum"
} 
function mean {
	  sum=$1
	  count=$2
         if (( count > 0 )); then
               echo "scale=3; $sum / $count" | bc
	else
	       echo "0"
	fi
}

function highest_grade {
	highest_grade=-1
	while IFS=',' read -r _ grade; do
		if [[ "$grade" == "Grade" ]]; then
			continue
		fi

		if (( grade > highest_grade ));then
			highest_grade=$grade
		fi
	done < "$1"
	echo "$highest_grade"
} 
function lowest_grade {
	lowest_grade=101
	while IFS=',' read -r _ grade; do
		if [[ "$grade" == "Grade" ]]; then
			continue
		fi
		if (( grade < lowest_grade )); then
			lowest_grade=$grade
		fi
	done < "$1"
	echo "$lowest_grade"
} 
csv_file="$1"
count=$(( $(cat "$csv_file" | wc -l) - 1 ))
sum_of_grades=$( sum_calculation "$csv_file" )
mean=$( mean "$sum_of_grades" "$count" )
highest_grade=$( highest_grade "$csv_file")
lowest_grade=$( lowest_grade "$csv_file")

echo "Number of grades: $count" 
echo "Sum of grades: $sum_of_grades"
echo "Mean of grades: $mean"
echo "Highest grade: $highest_grade"
echo "Lowest grade: $lowest_grade"


