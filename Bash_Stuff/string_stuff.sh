#!bin/bash
str="gargantuan"
offset=${str:4:3}
echo "$str"
echo "Length of string $str is ${#str}"
echo "$offset"
echo "Length of string $offset is ${#offset}"

original="braveheart"
end=${original:5:5}
echo "$original"
echo "Length of the string $original is ${#original}"
echo "$end"
echo "Length of the string $end is ${#end}"
