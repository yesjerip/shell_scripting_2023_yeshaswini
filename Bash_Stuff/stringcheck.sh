first=$1
second=$2
#regex
if [[ "$second" =~ .*$first.* ]];then
	echo "$first is a substring of $second (1.0)"
fi
#wildcards
if [[ "$second" == *"$first"* ]];then
	echo "$first is again a substring of $second (2.0)"
fi
