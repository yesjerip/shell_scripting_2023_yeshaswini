#! bin/bash
ONE="$1"
TWO="$2"

if [[ "2" -ne $# ]];then
	exit 1
elif [[ -z $1 ]];then
	exit 2
fi
	
if [[ "$ONE" == "$TWO"  ]];then
	exit 1
elif [[ "$ONE" > "$TWO" ]];then
	echo "$TWO is less than $ONE"
	echo "Length of the string one is ${#ONE}"
else
	echo "$ONE is less than $TWO"
	echo "Length of the string two is ${#TWO}"
fi

