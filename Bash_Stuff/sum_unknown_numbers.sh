#!/bin/bash

if [ $# -eq 0 ]; then
	    echo "Please provide floating-point numbers as arguments"
	        exit 1
fi

sum=0

# Loop through each argument and add it to the sum
for arg in "$@"; do
	    # Using bc to perform floating-point arithmetic
	    sum=$(echo "$sum + $arg" | bc)
		if [ $? -ne 0 ]; then
			echo "Error: '$arg' is not a valid floating-point number."
	        fi
done

echo "Sum of all floating point numbers is $sum"

