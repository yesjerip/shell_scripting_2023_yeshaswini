#!/bin/bash

date_into_seconds() {
	    date -d "$1" +"%s"
    }
    
starting_time=$(date +%s)
sleep 10
end_time=$(date +%s)
# Calculate the sleep duration
sleep_duration=$(($end_time - $starting_time))
# Convert seconds to minutes and seconds
minutes=$((sleep_duration / 60)) #not really necessary because we can't wait for several minutes to run the code.
seconds=$((sleep_duration % 60))

echo "Sleep sleeps for $minutes minutes and $seconds seconds."

