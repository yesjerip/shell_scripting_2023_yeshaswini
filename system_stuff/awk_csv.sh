#!/bin/bash

echo "Number of columns in each line:"
awk -F',' '{print NF}' pretend_data.csv
echo "Total number of rows:"
awk 'END {print NR}' pretend_data.csv

