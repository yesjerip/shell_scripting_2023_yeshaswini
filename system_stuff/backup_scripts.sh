#! /bin/bash
find -iname *.sh | xargs tar cJf bak.tar.xz

#J tells us to compress with xz compression
# for decompress the files use tar xJvf bak.tar.xz
#here v is verbose and x is for extracting
