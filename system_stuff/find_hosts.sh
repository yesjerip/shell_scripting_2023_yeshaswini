#! /bin/bash


function pingHostQuiet {
	    ping -c 1 -q -W 1 $1 1>/dev/null 2>/dev/null
	        echo $?
}

if [[ $# -lt 2 ]]; then
    echo "Run with $0 HOST INTERVAL"
	exit
fi

subnet_base=$1
interval=$2

count=0
alive=0

for host in {1..254}; do
       ip_address="${subnet_base}${host}"
       echo "Ping $ip_address"
       ((count++))
       ret=$(pingHostQuiet $ip_address)
if [[ $ret -eq 0 ]]; then
	echo "Host $ip_address is alive"
	((alive++))
else
       echo "Host $ip_address failed to respond to ping ($alive/$count)"
fi
done

echo "Found $alive alive hosts out of $count total hosts."


