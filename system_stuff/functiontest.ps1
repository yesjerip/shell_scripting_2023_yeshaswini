﻿function Test-Add {

    param(
          $first,   #if we want integer keep [int] before $first if we want float keep float
          $second
          )

          Return $first + $second 
}

function Test-Mult {

    param(
          $first,   #if we want integer keep [int] before $first if we want float keep float
          $second
          )

          Return $first * $second 
}




$result = Test-Add 10 20
Write-Host("Result is " + $result)



$result1 = Test-Mult 10 20
Write-Host("Result is " + $result1)