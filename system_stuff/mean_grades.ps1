﻿
if ($args.Length -ne 1) {
    Write-Host "Usage: mean_grades.ps1 <csv_file>"
    exit 1
}

# Read the CSV file
$grades = Import-Csv $args[0] -Header Name, Grade


$total = 0
$highest = [float]::MinValue
$lowest = [float]::MaxValue


foreach ($grade in $grades) {
    $gradeValue = [float]$grade.Grade
    $total += $gradeValue

    if ($gradeValue -gt $highest) {
        $highest = $gradeValue
    }

    if ($gradeValue -lt $lowest) {
        $lowest = $gradeValue
    }
}

$mean = $total / $grades.Length

Write-Host "Total grades: $($grades.Length)"
Write-Host "Total sum: $total"
Write-Host "Mean grade: $mean"
Write-Host "Highest grade: $highest"
Write-Host "Lowest grade: $lowest"
