#!/usr/bin/env python3

import re
import sys

test_strings = ["Denver, CO 80208", "George C. Wallace", "800-555-2424", "999-4653"]

# Match a city, state ZIP
city_regex = re.compile(r'\w+, [A-Z]{2} \d{5}')

# Match a full name with optional middle initial
name_regex = re.compile(r'\b[A-Z][a-z]+(?: [A-Z]\.)? [A-Z][a-z]+\b')

# Match a phone number
phone_regex = re.compile(r'(\d{3}-\d{3}-\d{4}|\d{3}-\d{4})')

def main():
        for line in test_strings:
                    # Check city match
            city_result = city_regex.match(line)
            if city_result:
                    print(f"Matched city regex on {line}")

                    # Check name match
                    name_result = name_regex.match(line)
            if name_result:
                    print(f"Matched name regex on {line}")

                    # Check phone number match
                    phone_result = phone_regex.match(line)
            if phone_result:
                    print(f"Matched phone regex on {line}")

            if _name_ == "_main_":
                    main()
