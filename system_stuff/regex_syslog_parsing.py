#!/usr/bin/env python3

import re
import sys
from collections import Counter


match_basic_syslog = re.compile(r'^(?P<date>\d{4}-\d{2}-\d{2}) (?P<ts>\d{2}:\d{2}:\d{2}.\d{9})\s(?P<data>.*)$')
match_invalid_user = re.compile(r'Invalid user (?P<username>\w+) from (?P<ip>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}) port (?P<port>\d+)')
match_disconnected_invalid = re.compile(r'Disconnected from invalid user (?P<user>\w+) (?P<ip>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})')
match_disconnected_authenticating = re.compile(r'Disconnected from authenticating user (?P<user>\w+) (?P<ip>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})')

def main():
        invalid_usernames = []
        invalid_ips = []
        authenticating_usernames = []
with open(sys.argv[1], 'r') as logfile:
    for line in logfile:
        curr_match = match_basic_syslog.match(line)
        if curr_match:
                  data = curr_match.groupdict()['data']
                  invalid_user_match = match_invalid_user.search(data)
        if invalid_user_match:
                  invalid_usernames.append(invalid_user_match.group('username'))
                  invalid_ips.append(invalid_user_match.group('ip'))
                  disconnected_invalid_match = match_disconnected_invalid.search(data)
        if disconnected_invalid_match:
                  invalid_usernames.append(disconnected_invalid_match.group('user'))
                  invalid_ips.append(disconnected_invalid_match.group('ip'))
                 disconnected_authenticating_match = match_disconnected_authenticating.search(data)                                                                                 if disconnected_authenticating_match:
                authenticating_usernames.append(disconnected_authenticating_match.group('user'))
                distinct_invalid_usernames = list(set(invalid_usernames))                                                                                                                   distinct_authenticating_usernames = list(set(authenticating_usernames))
                most_common_username = Counter(invalid_usernames).most_common(1)                                                                                                            most_common_ip = Counter(invalid_ips).most_common(1)

                                                                                                                                                                                                                                                                                                                                                        print(f"Distinct invalid usernames: {', '.join(distinct_invalid_usernames)}")
                                                                                                                                                                                                                                                                                                                                                        print(f"Distinct authenticating usernames: {', '.join(distinct_authenticating_usernames)}") 
                                                                                                                                                                                                                                                                                                                                                        print(f"Most common invalid username: {most_common_username[0][0]} with {most_common_username[0][1]} occurrences.") 
                                                                                                                                                                                                                                                                                                                                                        print(f"IP address with most invalid attempts: {most_common_ip[0][0]} with {most_common_ip[0][1]} attempts.")

                                                                                                                                                                                                                                                                                                                                                        if _name_ == "_main_":
                                                                                                                                                                            if len(sys.argv) > 1                                                                                                                                                                                                                                                                                                                                        main()
                                                                                                                                                                            else:
                                                                                                                                                                                sys.stderr.write("Must provide a file path as argument 1.")
