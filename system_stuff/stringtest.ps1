﻿# Check if there are two command line arguments
if ($args.Length -ne 2) {
    Write-Host "Usage: stringtest.ps1 <string1> <string2>"
    exit 1
}

# Extract command line arguments
$string1 = $args[0]
$string2 = $args[1]

# Check for equality
if ($string1 -eq $string2) {
    Write-Host "The two strings are equal."
} else {
    Write-Host "The two strings are not equal."
}

# Check for alphanumeric order
if ($string1 -lt $string2) {
    Write-Host "$string1 comes before $string2 in alphanumeric order."
} elseif ($string1 -gt $string2) {
    Write-Host "$string1 comes after $string2 in alphanumeric order."
} else {
    Write-Host "The two strings have the same alphanumeric order."
}
