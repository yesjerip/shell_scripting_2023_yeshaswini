﻿# Check if there are any command line arguments
if ($args.Length -eq 0) {
    Write-Host "Usage: sum_unknown_numbers.ps1 <number1> <number2> ..."
    exit 1
}

# Initialize the sum
$sum = 0

# Loop through command line arguments and sum them up
foreach ($arg in $args) {
    $number = [float]$arg
    $sum += $number
}

# Output the sum
Write-Host "Sum of the numbers: $sum"
