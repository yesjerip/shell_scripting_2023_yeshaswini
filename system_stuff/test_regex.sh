#!/bin/bash

#1 regex that matches zip code
echo "80014" | sed -n -e '/^[0-9]\{5\}$/p'

# 2. a US phone number including area code
echo "720-877-6666" | sed -n -e '/^[0-9]\{3\}-[0-9]\{3\}-[0-9]\{4\}$/p'

# 3. matches  a first or last name with a capital letter
echo "Yashu" | sed -n -e '/^[A-Z][a-zA-Z]*$/p'

# 4. Match an IPv4 address
echo "10.0.0.1" | sed -n -e '/^\([0-9]\{1,3\}\.\)\{3\}[0-9]\{1,3\}$/p'

# 5. Match a website URL 
echo "http://www.yahoo.com" | sed -n -e '/^http[s]\?:\/\/\([a-zA-Z0-9\-]\+\.\)*[a-zA-Z0-9\-]\+\.[a-zA-Z]\{2,6\}\/\?$/p'
