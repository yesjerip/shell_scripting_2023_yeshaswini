﻿$startTime = Get-Date
Start-Sleep -Seconds 5
$endTime = Get-Date
$duration = $endTime - $startTime
Write-Host "Sleep duration: $duration"
