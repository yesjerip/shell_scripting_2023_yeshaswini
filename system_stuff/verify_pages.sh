#!/bin/bash

# Check if the required arguments are provided
if [ $# -ne 1 ]; then
	    echo "Usage: $0 <json_file>"
	        exit 1
fi

json_file=$1

if [ ! -f "$json_file" ]; then
	    echo "JSON file not found: $json_file"
	        exit 1
fi


#urls=$(jq -r '.[].url' "$json_file")
#statuses=$(jq -r '.[].status' "$json_file")

# Function to verify the status
function verify_status() {
	 url=$1
	 expected_status=$2

         http_code=$(curl -s -o /dev/null -w "%{http_code}" "$url")

         if [[ "$http_code" -eq "$expected_status" ]]; then
		echo "SUCCESS: $url returned status $expected_status"
	else
		echo "FAILURE: $url returned status $http_code, expected $expected_status"
	fi
}

							
# Iterate through the lines of the JSON file and verify the status of each URL
							
while read -r line; do
   url=$(echo "$line" | jq -r '.url')
   expected_status=$(echo "$line" | jq -r '.status')
   verify_status "$url" "$expected_status"
done < "$json_file"

